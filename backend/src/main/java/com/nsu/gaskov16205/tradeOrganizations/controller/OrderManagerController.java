package com.nsu.gaskov16205.tradeOrganizations.controller;

import com.nsu.gaskov16205.tradeOrganizations.models.OrderManager;
import com.nsu.gaskov16205.tradeOrganizations.service.base.exception.NoIdentifierFoundException;
import com.nsu.gaskov16205.tradeOrganizations.service.orderManager.IOrderManagerService;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class OrderManagerController {
    //region DI
    private final IOrderManagerService managerService;

    public OrderManagerController(@Qualifier("SQLOrderManagerService") IOrderManagerService managerService) {
        this.managerService = managerService;
    }
    //endregion

    @PostMapping("/orderManager/add/{name}")
    public ResponseEntity addOrderManager(@PathVariable String name) {
        managerService.add(new OrderManager(name));
        return new ResponseEntity(HttpStatus.OK);
    }

    @GetMapping("/orderManager/{id}")
    public @ResponseBody ResponseEntity<OrderManager> getOrderById(@PathVariable String id) {
        try {
            return ResponseEntity.ok(managerService.getById(id));
        } catch (NoIdentifierFoundException e) {
            return ResponseEntity.notFound().build();
        }
    }

    @PutMapping("/orderManager/update")
    public ResponseEntity updateProduct(@RequestBody OrderManager orderManager) {
        try {
            managerService.update(orderManager);
        } catch (NoIdentifierFoundException e) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity(HttpStatus.OK);
    }

    @PostMapping("/orderManager/delete/{id}")
    public ResponseEntity deleteOrderManager(@PathVariable String id) {
        try {
            managerService.deleteById(id);
        } catch (NoIdentifierFoundException e) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity(HttpStatus.OK);
    }

    @GetMapping("/orderManager")
    public @ResponseBody ResponseEntity<List<OrderManager>> getOrderManagers() {
        return ResponseEntity.ok(managerService.getSorted());
    }
}
