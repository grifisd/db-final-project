package com.nsu.gaskov16205.tradeOrganizations.controller;

import com.nsu.gaskov16205.tradeOrganizations.models.Product;
import com.nsu.gaskov16205.tradeOrganizations.service.base.exception.NoIdentifierFoundException;
import com.nsu.gaskov16205.tradeOrganizations.service.product.IProductService;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class ProductController {
    //region DI
    private final IProductService productService;

    public ProductController(@Qualifier("SQLProductService") IProductService productService) {
        this.productService = productService;
    }
    //endregion

    @PostMapping("/product/add/{name}")
    public ResponseEntity addProduct(@PathVariable String name) {
        productService.add(new Product(name));
        return new ResponseEntity(HttpStatus.OK);
    }

    @GetMapping("/product/{id}")
    public @ResponseBody ResponseEntity<Product> getProductById(@PathVariable String id) {
        try {
            return ResponseEntity.ok(productService.getById(id));
        } catch (NoIdentifierFoundException e) {
            return ResponseEntity.notFound().build();
        }
    }

    @PutMapping("/product/update")
    public ResponseEntity updateProduct(@RequestBody Product product) {
        try {
            productService.update(product);
        } catch (NoIdentifierFoundException e) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity(HttpStatus.OK);
    }

    @PostMapping("/product/delete/{id}")
    public ResponseEntity deleteProduct(@PathVariable String id) {
        try {
            productService.deleteById(id);
        } catch (NoIdentifierFoundException e) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity(HttpStatus.OK);
    }

    @GetMapping("/product")
    public @ResponseBody ResponseEntity<List<Product>> getProducts() {
        return ResponseEntity.ok(productService.getSorted());
    }
}
