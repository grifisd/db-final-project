package com.nsu.gaskov16205.tradeOrganizations.controller;

import com.nsu.gaskov16205.tradeOrganizations.service.base.exception.NoIdentifierFoundException;
import com.nsu.gaskov16205.tradeOrganizations.service.product.IProductService;
import com.nsu.gaskov16205.tradeOrganizations.service.provider.IProviderService;
import lombok.extern.log4j.Log4j2;
import lombok.val;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.http.HttpStatus.OK;

@Log4j2
@RestController
public class ProductProviderController {
    //region DI
    private final IProductService productService;
    private final IProviderService providerService;

    public ProductProviderController(
            @Qualifier("SQLProductService") IProductService productService,
            @Qualifier("SQLProviderService") IProviderService providerService) {
        this.productService = productService;
        this.providerService = providerService;
    }
    //endregion

//    @PostMapping("/productProvider/add")
//    public ResponseEntity addProductProvider(@RequestBody ProductProvider productProvider) {
//        val x = 4;
//        try {
//            val product = productService.getById(productProvider.getProduct().getId());
//            val oldProviders = product.getProductProviders();
//            oldProviders.add(productProvider);
//            product.setProductProviders(oldProviders);
//            productService.update(product);
//        } catch (NoIdentifierFoundException e) {
//            return new ResponseEntity(HttpStatus.BAD_REQUEST);
//        }
//        return new ResponseEntity(HttpStatus.OK);
//    }
//    @PostMapping("/productProvider/delete/")
//    public ResponseEntity deleteProductProvider(
//            @RequestParam("productId") String productId,
//            @RequestParam("providerId") String providerId) {
//        try {
//            productProviderService.deleteById();
//        } catch (NoIdentifierFoundException e) {
//            return new ResponseEntity(HttpStatus.NOT_FOUND);
//        }
//        return new ResponseEntity(HttpStatus.OK);
//    }
//
    @GetMapping("/productProvider")
    public ResponseEntity getProductProviders() {
        try {
            val moloko = productService.getById("4a1ee448-1ab5-4e60-8fca-486c84a73999");
            val pyater = providerService.getById("d7f43ba5-ada6-49ad-aa30-8f2c6a3d6fc1");

            log.info(moloko);
            log.info(pyater);

        } catch (NoIdentifierFoundException e) {
            e.printStackTrace();
        }
        return new ResponseEntity(OK);
    }
}
