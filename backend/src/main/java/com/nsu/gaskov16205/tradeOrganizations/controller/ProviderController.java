package com.nsu.gaskov16205.tradeOrganizations.controller;

import com.nsu.gaskov16205.tradeOrganizations.models.Provider;
import com.nsu.gaskov16205.tradeOrganizations.service.base.exception.NoIdentifierFoundException;
import com.nsu.gaskov16205.tradeOrganizations.service.provider.IProviderService;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class ProviderController {
    private final IProviderService providerService;

    public ProviderController(@Qualifier("SQLProviderService") IProviderService providerService) {
        this.providerService = providerService;
    }

    @PostMapping("/provider/add/{name}")
    public ResponseEntity addManager(@PathVariable String name) {
        providerService.add(new Provider(name));
        return new ResponseEntity(HttpStatus.OK);
    }

    @GetMapping("/provider/{id}")
    public @ResponseBody ResponseEntity<Provider> getProviderById(@PathVariable String id) {
        try {
            return ResponseEntity.ok(providerService.getById(id));
        } catch (NoIdentifierFoundException e) {
            return ResponseEntity.notFound().build();
        }
    }

    @PutMapping("/provider/update")
    public ResponseEntity updateProvider(@RequestBody Provider provider) {
        try {
            providerService.update(provider);
        } catch (NoIdentifierFoundException e) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity(HttpStatus.OK);
    }

    @PostMapping("/provider/delete/{id}")
    public ResponseEntity deleteManager(@PathVariable String id) {
        try {
            providerService.deleteById(id);
        } catch (NoIdentifierFoundException e) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity(HttpStatus.OK);
    }

    @GetMapping("/provider")
    public @ResponseBody ResponseEntity<List<Provider>> getProviders() {
        return ResponseEntity.ok(providerService.getSorted());
    }
}