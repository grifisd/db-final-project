package com.nsu.gaskov16205.tradeOrganizations.controller;

import com.nsu.gaskov16205.tradeOrganizations.models.SectionManager;
import com.nsu.gaskov16205.tradeOrganizations.service.base.exception.NoIdentifierFoundException;
import com.nsu.gaskov16205.tradeOrganizations.service.sectionManager.ISectionManagerService;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class SectionManagerController {
    private final ISectionManagerService sectionManagerService;

    public SectionManagerController(@Qualifier("inMemorySectionManagerService") ISectionManagerService sectionManagerService) {
        this.sectionManagerService = sectionManagerService;
    }

    @PostMapping("/sectionManager/add/{name}")
    public ResponseEntity addSectionManager(@PathVariable String name) {
        sectionManagerService.add(new SectionManager(name));
        return new ResponseEntity(HttpStatus.OK);
    }


    @GetMapping("/sectionManager/{id}")
    public @ResponseBody ResponseEntity<SectionManager> getSectionManagerById(@PathVariable String id) {
        try {
            return ResponseEntity.ok(sectionManagerService.getById(id));
        } catch (NoIdentifierFoundException e) {
            return ResponseEntity.notFound().build();
        }
    }

    @PutMapping("/sectionManager/update")
    public ResponseEntity updateSectionManager(@RequestBody SectionManager sectionManager) {
        try {
            sectionManagerService.update(sectionManager);
        } catch (NoIdentifierFoundException e) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity(HttpStatus.OK);
    }

    @PostMapping("/sectionManager/delete/{id}")
    public ResponseEntity deleteSectionManager(@PathVariable String id) {
        try {
            sectionManagerService.deleteById(id);
        } catch (NoIdentifierFoundException e) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity(HttpStatus.OK);
    }

    @GetMapping("/sectionManager")
    public @ResponseBody ResponseEntity<List<SectionManager>> getSectionManagers() {
        return ResponseEntity.ok(sectionManagerService.getSorted());
    }
}
