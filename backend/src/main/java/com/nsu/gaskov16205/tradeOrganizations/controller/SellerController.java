package com.nsu.gaskov16205.tradeOrganizations.controller;

import com.nsu.gaskov16205.tradeOrganizations.models.Seller;
import com.nsu.gaskov16205.tradeOrganizations.service.base.exception.NoIdentifierFoundException;
import com.nsu.gaskov16205.tradeOrganizations.service.seller.ISellerService;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class SellerController {
    private final ISellerService sellerService;

    public SellerController(@Qualifier("SQLSellerService") ISellerService sellerService) {
        this.sellerService = sellerService;
    }

    @PostMapping("/seller/add/{name}")
    public ResponseEntity addManager(@PathVariable String name) {
        sellerService.add(new Seller(name));
        return new ResponseEntity(HttpStatus.OK);
    }
    
    @GetMapping("/seller/{id}")
    public @ResponseBody ResponseEntity<Seller> getSellerById(@PathVariable String id) {
        try {
            return ResponseEntity.ok(sellerService.getById(id));
        } catch (NoIdentifierFoundException e) {
            return ResponseEntity.notFound().build();
        }
    }

    @PutMapping("/seller/update")
    public ResponseEntity updateSeller(@RequestBody Seller seller) {
        try {
            sellerService.update(seller);
        } catch (NoIdentifierFoundException e) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity(HttpStatus.OK);
    }

    @PostMapping("/seller/delete/{id}")
    public ResponseEntity deleteManager(@PathVariable String id) {
        try {
            sellerService.deleteById(id);
        } catch (NoIdentifierFoundException e) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity(HttpStatus.OK);
    }

    @GetMapping("/seller")
    public @ResponseBody ResponseEntity<List<Seller>> getSellers() {
        return ResponseEntity.ok(sellerService.getSorted());
    }
}
