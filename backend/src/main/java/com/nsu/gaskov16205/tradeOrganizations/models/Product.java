package com.nsu.gaskov16205.tradeOrganizations.models;

import com.nsu.gaskov16205.tradeOrganizations.models.base.SingleIdModel;
import lombok.*;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(exclude = "name", callSuper = true)

@Entity
public class Product extends SingleIdModel implements Comparable<Product> {
    private String name;
    private Set<ProductProvider> productProviders = new HashSet<>();

    public Product(String name, ProductProvider... productProviders) {
        this.name = name;

        for (ProductProvider productProvider : productProviders)
            productProvider.setProduct(this);
        this.productProviders = Stream.of(productProviders).collect(Collectors.toSet());
    }

    @Override
    public int compareTo(Product product) {
        return name.compareTo(product.name);
    }

    @OneToMany(mappedBy = "product", cascade = CascadeType.ALL)
    public Set<ProductProvider> getProductProviders() {
        return productProviders;
    }

    @Column
    public String getName() {
        return name;
    }

    @Id
    @Column
    public String getId() {
        return super.getId();
    }
}
