package com.nsu.gaskov16205.tradeOrganizations.models;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;

@Getter
@Setter
//@ToString
@NoArgsConstructor
@AllArgsConstructor
//@EqualsAndHashCode(of = {"product", "provider"})

@Entity
public class ProductProvider implements Serializable {
    @Id
    @ManyToOne
    @JoinColumn
    private Product product;

    @Id
    @ManyToOne
    @JoinColumn
    private Provider provider;

    @Column double price;

    public ProductProvider(Provider provider, double price) {
        this.provider = provider;
        this.price = price;
    }
}
