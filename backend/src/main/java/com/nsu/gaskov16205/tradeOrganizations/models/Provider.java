package com.nsu.gaskov16205.tradeOrganizations.models;

import com.nsu.gaskov16205.tradeOrganizations.models.base.SingleIdModel;
import lombok.*;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(exclude = "name", callSuper = true)

@Entity
public class Provider extends SingleIdModel implements Comparable<Provider> {
    private String name;
    private Set<ProductProvider> productProviders = new HashSet<>();

    @Id
    @Column
    public String getId() {
        return super.getId();
    }

    @Column
    public String getName() {
        return name;
    }

    @OneToMany(mappedBy = "provider", cascade = CascadeType.ALL)
    public Set<ProductProvider> getProductProviders() {
        return productProviders;
    }

    public Provider(String name) {
        this.name = name;
    }

    @Override
    public int compareTo(Provider provider) {
        return name.compareTo(provider.name);
    }
}
