package com.nsu.gaskov16205.tradeOrganizations.models;

import com.nsu.gaskov16205.tradeOrganizations.models.base.SingleIdModel;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)

@Entity
public class Seller extends SingleIdModel implements Comparable<Seller> {
    private String name;

    @Override
    public int compareTo(Seller seller) {
        return name.compareTo(seller.name);
    }

    @Column
    public String getName() {
        return name;
    }

    @Id
    @Column
    public String getId() {
        return super.getId();
    }

}
