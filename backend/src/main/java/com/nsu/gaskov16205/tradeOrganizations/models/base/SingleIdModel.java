package com.nsu.gaskov16205.tradeOrganizations.models.base;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import java.util.UUID;

@Getter
@Setter
@AllArgsConstructor
@EqualsAndHashCode(of = "id")
public class SingleIdModel {
    private String id;

    protected SingleIdModel() {
        id = UUID.randomUUID().toString();
    }
}
