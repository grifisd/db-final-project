package com.nsu.gaskov16205.tradeOrganizations.service.base;

import com.nsu.gaskov16205.tradeOrganizations.models.base.SingleIdModel;
import com.nsu.gaskov16205.tradeOrganizations.service.base.exception.NoIdentifierFoundException;
import lombok.val;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public interface BaseCrudService<T extends SingleIdModel & Comparable<T>> {

    //region Create
    default void add(T value) {
        getMutable().add(value);
    }
    //endregion

    //region Read
    //TODO: test
    default T getById(String id) throws NoIdentifierFoundException {
        return getImmutable()
                .stream()
                .filter(t -> t.getId().equals(id))
                .findAny()
                .orElseThrow(NoIdentifierFoundException::new);
    }

    Set<T> getMutable();

    default Set<T> getImmutable() {
        val mutable = getMutable();
        return new HashSet<>(mutable);
    }

    default List<T> getSorted() {
        return getImmutable()
                .stream()
                .sorted()
                .collect(Collectors.toList());
    }
    //endregion

    //region Update
    default void update(T value) throws NoIdentifierFoundException {
        delete(value);
        getMutable().add(value);
    }
    //endregion

    //region Delete
    //TODO: test on mutableCollections
    default void deleteById(String id) throws NoIdentifierFoundException {
        boolean isDeleted = getMutable().removeIf(t -> t.getId().equals(id));
        if (!isDeleted)
            throw new NoIdentifierFoundException();
    }

    default void delete(T value) throws NoIdentifierFoundException {
        boolean isDeleted = getMutable().remove(value);
        if (!isDeleted)
            throw new NoIdentifierFoundException();
    }
    //endregion
}
