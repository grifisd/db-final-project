package com.nsu.gaskov16205.tradeOrganizations.service.base;

import com.nsu.gaskov16205.tradeOrganizations.models.base.SingleIdModel;
import com.nsu.gaskov16205.tradeOrganizations.service.base.exception.SQLNoMutableCollectionException;
import lombok.val;
import org.springframework.data.repository.CrudRepository;

import java.util.HashSet;
import java.util.Set;

public abstract class BaseSQLCrudService<T extends SingleIdModel & Comparable<T>> implements BaseCrudService<T> {
    protected abstract CrudRepository<T, String> getRepository();

    //region Create
    @Override
    public void add(T value) {
        getRepository().save(value);
    }
    //endregion

    //region Read
    @Override
    public Set<T> getMutable() {
        throw new SQLNoMutableCollectionException(this);
    }

    @Override
    public Set<T> getImmutable() {
        val all = getRepository().findAll();
        val immutableSet = new HashSet<T>();
        for (T value : all) {
            immutableSet.add(value);
        }
        return immutableSet;
    }
    //endregion

    //region Update
    @Override
    public void update(T value) {
        getRepository().delete(value);
        getRepository().save(value);
    }
    //endregion

    //region Delete
    @Override
    public void delete(T value) {
        getRepository().delete(value);
    }

    @Override
    public void deleteById(String id) {
        getRepository().deleteById(id);
    }

    //endregion
}