package com.nsu.gaskov16205.tradeOrganizations.service.base.exception;

public class SQLNoMutableCollectionException extends RuntimeException {
    public SQLNoMutableCollectionException(Object object) {
        super(object.getClass().getCanonicalName());
    }
}
