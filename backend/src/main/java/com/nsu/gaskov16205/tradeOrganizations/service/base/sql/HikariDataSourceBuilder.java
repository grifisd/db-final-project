//package com.nsu.gaskov16205.tradeOrganizations.service.base.sql;
//
//import com.zaxxer.hikari.HikariDataSource;
//import org.hibernate.SessionFactory;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
//
//import javax.persistence.EntityManagerFactory;
//import javax.sql.DataSource;
//
//public class HikariDataSourceBuilder {
////    @Bean
////    @ConfigurationProperties("application.properties")
////    public HikariDataSource dataSource() {
////        return DataSourceBuilder.create().type(HikariDataSource.class).build();
////    }
//
//    @Bean
//    public DataSource dataSource() {
//        HikariDataSource ds = new HikariDataSource();
//        ds.setMaximumPoolSize(100);
//        ds.setDataSourceClassName("com.mysql.jdbc.jdbc2.optional.MysqlDataSource");
//        ds.addDataSourceProperty("url", "jdbc:mysql://localhost:3306/test");
//        ds.addDataSourceProperty("user", "root");
//        ds.addDataSourceProperty("password", "password");
//        ds.addDataSourceProperty("cachePrepStmts", true);
//        ds.addDataSourceProperty("prepStmtCacheSize", 250);
//        ds.addDataSourceProperty("prepStmtCacheSqlLimit", 2048);
//        ds.addDataSourceProperty("useServerPrepStmts", true);
//        return ds;
//    }
//
//    @Bean
//    public EntityManagerFactory entityManagerFactory() {
//        LocalContainerEntityManagerFactoryBean emf = new LocalContainerEntityManagerFactoryBean();
//        emf.setDataSource(dataSource());
//        //emf.setJpaVendorAdapter(jpaVendorAdapter);
//        emf.setPackagesToScan("com.nsu.gaskov16205.tradeOrganizations.models");
//        emf.setPersistenceUnitName("default");
//        emf.afterPropertiesSet();
//        return emf.getObject();
//    }
//
//    @Configuration
//    public class BeanConfig {
//        //region DI
//        private final EntityManagerFactory entityManagerFactory;
//
//        public BeanConfig(EntityManagerFactory entityManagerFactory) {
//            this.entityManagerFactory = entityManagerFactory;
//        }
//        //endregion
//
//        @Bean
//        public SessionFactory getSessionFactory() {
//            if (entityManagerFactory.unwrap(SessionFactory.class) == null) {
//                throw new RuntimeException("Factory is not a Hibernate factory");
//            }
//            return entityManagerFactory.unwrap(SessionFactory.class);
//        }
//    }
//}
