package com.nsu.gaskov16205.tradeOrganizations.service.orderManager;

import com.nsu.gaskov16205.tradeOrganizations.models.OrderManager;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

@Service
public class FakeOrderManagerService implements IOrderManagerService {
    @Override
    public Set<OrderManager> getMutable() {
        Set<OrderManager> s = new HashSet<>();
        s.add(new OrderManager("fakeManager"));
        return s;
    }
}
