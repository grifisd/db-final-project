package com.nsu.gaskov16205.tradeOrganizations.service.orderManager;

import com.nsu.gaskov16205.tradeOrganizations.models.OrderManager;
import com.nsu.gaskov16205.tradeOrganizations.service.base.BaseCrudService;

public interface IOrderManagerService extends BaseCrudService<OrderManager> {
}
