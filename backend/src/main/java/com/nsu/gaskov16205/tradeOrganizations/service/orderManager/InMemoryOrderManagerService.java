package com.nsu.gaskov16205.tradeOrganizations.service.orderManager;

import com.nsu.gaskov16205.tradeOrganizations.models.OrderManager;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

@Service
public class InMemoryOrderManagerService implements IOrderManagerService {
    private final Set<OrderManager> orderManagers = new HashSet<>();

    @Override
    public Set<OrderManager> getMutable() {
        return orderManagers;
    }
}
