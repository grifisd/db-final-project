package com.nsu.gaskov16205.tradeOrganizations.service.orderManager;

import com.nsu.gaskov16205.tradeOrganizations.models.OrderManager;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OrderManagerRepository extends CrudRepository<OrderManager, String> {}
