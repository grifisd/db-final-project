package com.nsu.gaskov16205.tradeOrganizations.service.orderManager;

import com.nsu.gaskov16205.tradeOrganizations.models.OrderManager;
import com.nsu.gaskov16205.tradeOrganizations.service.base.BaseSQLCrudService;
import lombok.Getter;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
@Transactional
public class SQLOrderManagerService extends BaseSQLCrudService<OrderManager> implements IOrderManagerService {
    //region @DI
    @Getter private OrderManagerRepository repository;

    public SQLOrderManagerService(OrderManagerRepository repository) {
        this.repository = repository;
    }
    //endregion
}