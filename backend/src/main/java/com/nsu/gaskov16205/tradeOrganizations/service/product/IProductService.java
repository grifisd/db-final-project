package com.nsu.gaskov16205.tradeOrganizations.service.product;

import com.nsu.gaskov16205.tradeOrganizations.service.base.BaseCrudService;
import com.nsu.gaskov16205.tradeOrganizations.models.Product;

public interface IProductService extends BaseCrudService<Product> {
}
