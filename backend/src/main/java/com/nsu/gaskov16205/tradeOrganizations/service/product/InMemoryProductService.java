package com.nsu.gaskov16205.tradeOrganizations.service.product;

import com.nsu.gaskov16205.tradeOrganizations.models.Product;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

@Service
public class InMemoryProductService implements IProductService {
    private final Set<Product> products = new HashSet<>();

    @Override
    public Set<Product> getMutable() {
        return products;
    }
}
