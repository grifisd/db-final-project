package com.nsu.gaskov16205.tradeOrganizations.service.product;

import com.nsu.gaskov16205.tradeOrganizations.models.Product;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductRepository extends CrudRepository<Product, String> {}
