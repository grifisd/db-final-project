package com.nsu.gaskov16205.tradeOrganizations.service.product;


import com.nsu.gaskov16205.tradeOrganizations.models.Product;
import com.nsu.gaskov16205.tradeOrganizations.service.base.BaseSQLCrudService;
import lombok.Getter;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
@Transactional
public class SQLProductService extends BaseSQLCrudService<Product> implements IProductService {
    //region @DI
    @Getter private ProductRepository repository;

    public SQLProductService(ProductRepository repository) {
        this.repository = repository;
    }
    //endregion

    @Autowired SessionFactory sessionFactory;

    void f() {
        sessionFactory.getCurrentSession().createSQLQuery("SELECT * from Product");
    }
}