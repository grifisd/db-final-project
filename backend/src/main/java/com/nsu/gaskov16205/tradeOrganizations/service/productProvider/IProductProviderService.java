package com.nsu.gaskov16205.tradeOrganizations.service.productProvider;

import com.nsu.gaskov16205.tradeOrganizations.models.ProductProvider;

public interface IProductProviderService {
    void add(ProductProvider productProvider);
    void delete(ProductProvider productProvider);
}
