//package com.nsu.gaskov16205.tradeOrganizations.service.productProvider;
//
//import com.nsu.gaskov16205.tradeOrganizations.models.ProductProvider;
//import org.springframework.stereotype.Service;
//
//import java.util.HashSet;
//import java.util.Set;
//
//@Service
//public class InMemoryProductProviderService implements IProductProviderService {
//    private final Set<ProductProvider> providerProducts = new HashSet<>();
//
//    @Override
//    public Set<ProductProvider> getMutable() {
//        return providerProducts;
//    }
//}
