//package com.nsu.gaskov16205.tradeOrganizations.service.productProvider;
//
//import com.nsu.gaskov16205.tradeOrganizations.models.ProductProvider;
//import com.nsu.gaskov16205.tradeOrganizations.service.base.BaseSQLCrudService;
//import lombok.Getter;
//import org.springframework.stereotype.Service;
//
//import javax.transaction.Transactional;
//
//@Service
//@Transactional
//public class SQLProductProviderService extends BaseSQLCrudService<ProductProvider> implements IProductProviderService {
//    //region @DI
//    @Getter private ProductProviderRepository repository;
//
//    public SQLProductProviderService(ProductProviderRepository repository) {
//        this.repository = repository;
//    }
//    //endregion
//}