package com.nsu.gaskov16205.tradeOrganizations.service.provider;

import com.nsu.gaskov16205.tradeOrganizations.service.base.BaseCrudService;
import com.nsu.gaskov16205.tradeOrganizations.models.Provider;

public interface IProviderService extends BaseCrudService<Provider> {
}
