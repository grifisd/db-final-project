package com.nsu.gaskov16205.tradeOrganizations.service.provider;

import com.nsu.gaskov16205.tradeOrganizations.models.Provider;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

@Service
public class InMemoryProviderService implements IProviderService {
    private final Set<Provider> providers = new HashSet<>();

    @Override
    public Set<Provider> getMutable() {
        return providers;
    }
}
