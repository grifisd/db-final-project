package com.nsu.gaskov16205.tradeOrganizations.service.provider;

import com.nsu.gaskov16205.tradeOrganizations.models.Provider;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProviderRepository extends CrudRepository<Provider, String> {}
