package com.nsu.gaskov16205.tradeOrganizations.service.provider;


import com.nsu.gaskov16205.tradeOrganizations.models.Provider;
import com.nsu.gaskov16205.tradeOrganizations.service.base.BaseSQLCrudService;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
@Transactional
@RequiredArgsConstructor
public class SQLProviderService extends BaseSQLCrudService<Provider> implements IProviderService {
    @Getter private final ProviderRepository repository;
}