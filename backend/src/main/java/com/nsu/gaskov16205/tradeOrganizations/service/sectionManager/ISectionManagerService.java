package com.nsu.gaskov16205.tradeOrganizations.service.sectionManager;

import com.nsu.gaskov16205.tradeOrganizations.service.base.BaseCrudService;
import com.nsu.gaskov16205.tradeOrganizations.models.SectionManager;

public interface ISectionManagerService extends BaseCrudService<SectionManager> {
}
