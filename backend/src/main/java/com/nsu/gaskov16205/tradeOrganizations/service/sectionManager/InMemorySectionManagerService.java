package com.nsu.gaskov16205.tradeOrganizations.service.sectionManager;

import com.nsu.gaskov16205.tradeOrganizations.models.SectionManager;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

@Service
public class InMemorySectionManagerService implements ISectionManagerService {
    private final Set<SectionManager> sectionManagers = new HashSet<>();

    @Override
    public Set<SectionManager> getMutable() {
        return sectionManagers;
    }
}
