package com.nsu.gaskov16205.tradeOrganizations.service.seller;

import com.nsu.gaskov16205.tradeOrganizations.service.base.BaseCrudService;
import com.nsu.gaskov16205.tradeOrganizations.models.Seller;

public interface ISellerService extends BaseCrudService<Seller> {
}
