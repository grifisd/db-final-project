package com.nsu.gaskov16205.tradeOrganizations.service.seller;

import com.nsu.gaskov16205.tradeOrganizations.models.Seller;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

@Service
public class InMemorySellerService implements ISellerService {
    private final Set<Seller> sellers = new HashSet<>();

    @Override
    public Set<Seller> getMutable() {
        return sellers;
    }
}
