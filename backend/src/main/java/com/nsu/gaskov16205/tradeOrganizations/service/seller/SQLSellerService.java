package com.nsu.gaskov16205.tradeOrganizations.service.seller;

import com.nsu.gaskov16205.tradeOrganizations.models.Seller;
import com.nsu.gaskov16205.tradeOrganizations.service.base.BaseSQLCrudService;
import lombok.Getter;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
@Transactional
public class SQLSellerService extends BaseSQLCrudService<Seller> implements ISellerService {
    //region @DI
    @Getter private SellerRepository repository;

    public SQLSellerService(SellerRepository repository) {
        this.repository = repository;
    }
    //endregion
}