package com.nsu.gaskov16205.tradeOrganizations.service.seller;

import com.nsu.gaskov16205.tradeOrganizations.models.Seller;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SellerRepository extends CrudRepository<Seller, String> {}
